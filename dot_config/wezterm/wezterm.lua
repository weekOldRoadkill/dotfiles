-- Imports
local wezterm = require("wezterm")

-- Focus the window when the bell rings
wezterm.on("bell", function(window, pane)
	window:focus()
end)

-- Don't change the window title
wezterm.on("format-window-title", function(tab, pane, tabs, panes, config)
	return "WezTerm"
end)

-- Start the terminal maximized
wezterm.on("gui-startup", function(cmd)
	local tab, pane, window = wezterm.mux.spawn_window(cmd or {})
	window:gui_window():maximize()
end)

-- Set the font size and padding based on the window size
wezterm.on("window-resized", function(window, pane)
	local dims = window:get_dimensions()
	local font_size = 0.5 * (dims.pixel_width + dims.pixel_height) / dims.dpi
	local padding = math.floor(2 * font_size)
	window:set_config_overrides({
		font_size = math.floor(font_size),
		window_padding = { bottom = padding, left = padding, right = padding, top = padding },
	})
end)

-- Config Table
local default_prog
if wezterm.target_triple:match("windows") then
	default_prog = { "wsl", "--cd", "~" }
end
return {
	adjust_window_size_when_changing_font_size = false,
	animation_fps = 1,
	bold_brightens_ansi_colors = "No",
	color_scheme = "Gruvbox dark, hard (base16)",
	cursor_blink_ease_in = "Constant",
	cursor_blink_ease_out = "Constant",
	default_cursor_style = "BlinkingBlock",
	default_prog = default_prog,
	font = wezterm.font({
		family = "JetBrainsMonoNL Nerd Font Mono",
		harfbuzz_features = { "calt=0", "clig=0", "liga=0" },
	}),
	front_end = "WebGpu",
	hide_tab_bar_if_only_one_tab = true,
	max_fps = 255,
	native_macos_fullscreen_mode = true,
	warn_about_missing_glyphs = false,
	webgpu_power_preference = "HighPerformance",
}
