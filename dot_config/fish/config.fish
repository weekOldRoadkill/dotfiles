# Environment Variables
if type -q nvim # Use neovim, vim, or vi as the default editor
    set -gx EDITOR nvim
else if type -q vim
    set -gx EDITOR vim
else if type -q vi
    set -gx EDITOR vi
end
set -gx VISUAL $EDITOR # Use the same editor for VISUAL
if type -q less # Use less as the pager for man
    set -gx LESS -R --use-color -Dd+r\$Du+b\$
    set -gx MANPAGER less
    set -gx MANROFFOPT -P -c
end
if type -q npm # npm global path
    npm config set prefix $HOME/.npm-global
end
set -gx XDG_CONFIG_HOME $HOME/.config # XDG config dir
if type -q git && type -q diff # Git delta config
    git config --global core.pager "delta --dark -n --syntax-theme=gruvbox-dark"
    git config --global interactive.diffFilter "delta --color-only"
    git config --global delta.navigate true
    git config --global merge.conflictStyle zdiff3
end

# Paths
fish_add_path -P -m $HOME/.cargo/bin
fish_add_path -P -m $HOME/.local/bin
fish_add_path -P -m $HOME/.npm-global/bin
fish_add_path -P -m $HOME/go/bin

# Global Variables
set -g fish_prompt_pwd_full_dirs 64 # Show full directories in the prompt


# Abbreviations
abbr -a brew-list brew bundle dump --file - # List manually installed brew packages
abbr -a chez-edit chezmoi edit --apply # Edit config files managed by chezmoi
abbr -a cp cp -R # Copy directories recursively
abbr -a cpf cp -fR # Force copy directories recursively
abbr -a curl curl -LJO # Download a file with curl
abbr -a edit $EDITOR # Edit a file with the default editor
abbr -a gpg-reload "echo RELOADAGENT | gpg-connect-agent" # Reload the GPG agent
abbr -a grep grep -Ei # Case-insensitive grep
abbr -a grepo grep -Eio # Case-insensitive grep with only the match
abbr -a la ls -A # List all files
abbr -a ln ln -s # Create a symbolic link
abbr -a lnf ln -fs # Force create a symbolic link
abbr -a mkdir mkdir -p # Create directories recursively
abbr -a mvf mv -f # Force move files
abbr -a rm rm -r # Remove directories recursively
abbr -a rmf rm -fr # Force remove directories recursively
abbr -a tree tree -aC -I .git --gitignore # List files in a tree-like format
abbr -a zip zip -r9 # Recursively zip files with maximum compression


# Key bindings
bind \eb "fish_commandline_prepend background"
bind \eh "commandline home; commandline -f execute"
bind \et "commandline trace; commandline -f execute"
bind \ew "fish_commandline_prepend winrun"


# First time setup
function setup
    sudo-copy $HOME/.config/misc/fail2ban-filter-caddy-status /etc/fail2ban/filter.d/caddy-status.conf
    sudo-copy $HOME/.config/misc/fail2ban-jail-local /etc/fail2ban/jail.local
    sudo-copy $HOME/.config/misc/pam-sudo-local /etc/pam.d/sudo_local
    if not test -d $HOME/.tmux/plugins/tpm && type -q tmux
        git clone --recursive https://github.com/tmux-plugins/tpm $HOME/.tmux/plugins/tpm
    end
    if not type -q rustup
        curl --proto =https --tlsv1.2 -sSf https://sh.rustup.rs | sh
    end
    if not type -q nvim
        pushd (mktemp -d)
        git clone https://github.com/plctlab/LuaJIT
        pushd LuaJIT
        make
        sudo make install
        popd
        git clone https://github.com/neovim/neovim
        pushd neovim
        git checkout v0.10.3
        make CMAKE_BUILD_TYPE=Release BUNDLED_CMAKE_FLAG="\
            -DUSE_BUNDLED=OFF \
            -DUSE_BUNDLED_LUV=ON \
            -DUSE_BUNDLED_TS=ON \
            -DUSE_BUNDLED_LIBVTERM=ON \
            -DUSE_BUNDLED_LIBUV=ON \
            -DUSE_BUNDLED_LPEG=ON \
            -DUSE_BUNDLED_MSGPACK=ON \
            -DUSE_BUNDLED_UNIBILIUM=ON"
        sudo make install
        popd
        popd
    end
end

# Update system
function update
    setup
    set -l pkg_managers \
        apt \
        brew \
        cargo \
        chezmoi \
        go \
        npm \
        nvim \
        pipx \
        rustup \
        scoop \
        tpm \
        winget
    for pkg_manager in $pkg_managers
        for i in (seq $COLUMNS)
            echo -n -
        end
        echo -e "\n* $pkg_manager *"
        {$pkg_manager}-update
        {$pkg_manager}-defaults
        {$pkg_manager}-clean
    end
    return 0
end

# APT (Debian/Ubuntu)
function apt-update
    if type -q apt-get && type -q apt
        sudo apt update
        sudo apt upgrade
    end
end
function apt-defaults
    if type -q apt-get && type -q apt
        set -l try_pkgs \
            binfmt-support \
            btop \
            cmake \
            curl \
            docker-compose-v2 \
            entr \
            fail2ban \
            fastfetch \
            fish \
            g++ \
            gettext \
            git \
            git-delta \
            golang \
            libssh-dev \
            make \
            ninja-build \
            npm \
            pipx \
            pkg-config \
            python3 \
            qemu-user-static \
            ssh \
            sudo \
            tmux \
            tree \
            ufw \
            unzip \
            wbritish \
            wget
        set -l pkgs
        for pkg in $try_pkgs
            if apt show $pkg &>/dev/null
                set -a pkgs $pkg
            else
                echo $pkg not found
            end
        end
        sudo apt install $pkgs
    end
end
function apt-clean
    if type -q apt-get && type -q apt
        sudo apt autoremove
    end
end

# Homebrew (macOS)
function brew-update
    if type -q brew
        brew upgrade
    end
end
function brew-defaults
    if type -q brew
        brew bundle --global
    end
end
function brew-clean
    if type -q brew
        brew autoremove
        brew cleanup
        brew doctor
        brew bundle cleanup --global
    end
end

# Cargo (Rust)
function cargo-update
    if type -q cargo
        cargo install-update -a
    end
end
function cargo-defaults
    if type -q cargo
        set -l pkgs \
            "--git https://github.com/wgsl-analyzer/wgsl-analyzer wgsl_analyzer" \
            cargo-edit \
            cargo-update \
            stylua \
            taplo-cli \
            wasm-opt \
            wasm-pack
        for pkg in $pkgs
            cargo install (string split " " -- $pkg)
        end
    end
end
function cargo-clean
end

# Chezmoi (dotfiles)
function chezmoi-update
    if type -q chezmoi
        chezmoi upgrade
        chezmoi update
    end
    if type -q chezmoi.exe && type -q powershell.exe
        winrun chezmoi upgrade
        winrun chezmoi update
    end
end
function chezmoi-defaults
end
function chezmoi-clean
end

# Go (Go)
function go-update
end
function go-defaults
    if type -q go
        go install github.com/jesseduffield/lazygit@latest
    end
end
function go-clean
end

# npm (JavaScript)
function npm-update
    if type -q npm
        npm update -g
    end
end
function npm-defaults
    if type -q npm
        set -l pkgs \
            prettier
        npm install -g $pkgs
    end
end
function npm-clean
end

# Neovim
function nvim-update
    if type -q nvim
        nvim -c "lua update()"
    end
end
function nvim-defaults
end
function nvim-clean
end

# pipx (Python)
function pipx-update
    if type -q pipx
        pipx upgrade-all
    end
end
function pipx-defaults
    if type -q pipx
        set -l pkgs \
            speedtest-cli
        for pkg in $pkgs
            pipx install $pkg
        end
    end
end
function pipx-clean
end

# Rustup (Rust)
function rustup-update
    if type -q rustup
        rustup update
    end
    if type -q rustup.exe && type -q powershell.exe
        winrun rustup update
    end
end
function rustup-defaults
    if type -q rustup
        rustup component add rust-analyzer
    end
end
function rustup-clean
end

# Scoop (Windows)
function scoop-update
    if type -q scoop
        scoop update -a
    end
end
function scoop-defaults
    if type -q scoop
        set -l buckets \
            extras \
            games \
            main \
            nerd-fonts \
            versions
        for bucket in $buckets
            scoop bucket add $bucket
        end
        set -l pkgs \
            7zip \
            JetBrainsMono-NF-Mono \
            chezmoi \
            ezunlock \
            git \
            gsudo \
            mingw \
            speccy \
            vcredist \
            vlc \
            wezterm \
            wiztree
        for pkg in $pkgs
            scoop install $pkg
        end
    end
end
function scoop-clean
end

# Tmux Plugin Manager
function tpm-update
    set -l tpm $HOME/.tmux/plugins/tpm/bin
    if test -e $tpm/update_plugins -a -e $tpm/clean_plugins
        $tpm/update_plugins all
        $tpm/clean_plugins
    end
end
function tpm-defaults
    set -l tpm $HOME/.tmux/plugins/tpm/bin
    if test -e $tpm/install_plugins
        $tpm/install_plugins
    end
end
function tpm-clean
end

# WinGet (Windows)
function winget-update
end
function winget-defaults
    if type -q winget
        set -l pkgs \
            ATLauncher.ATLauncher \
            Balena.Etcher \
            Discord.Discord \
            Mozilla.Firefox \
            Valve.Steam
        for pkg in $pkgs
            winget install $pkg
        end
    end
end
function winget-clean
end


# Run a command in the background
function background
    fish -c "$argv" &>/dev/null & disown
end

# Attach to a docker container
function docker-attach
    sudo docker logs $argv[1]
    sudo docker attach $argv[1]
end

# Clean all docker containers and images
alias docker-clean "sudo docker system prune -af --volumes"

# List all docker containers
alias docker-list "sudo docker container ls -a"

# Fix open command in WSL
if type -q powershell.exe
    alias open "winrun start"
end

# Fix poweroff command in WSL
if type -q powershell.exe && type -q wsl.exe
    alias poweroff "winrun wsl --shutdown"
end

# Fix scoop command in WSL
if type -q powershell.exe && type -q scoop
    alias scoop "winrun scoop"
end

# Copy files with sudo
function sudo-copy
    if not cmp -s $argv[1] $argv[2]
        echo (prompt_pwd $argv[2]) changed, overwriting...
        if not test -e $argv[2]
            sudo mkdir -p (dirname $argv[2])
            sudo touch $argv[2]
        end
        sudo cp -R $argv[1] $argv[2]
    else
        echo (prompt_pwd $argv[2]) unchanged
    end
end

# Toggle fish trace
function trace
    if test -z "$fish_trace"
        set -g fish_trace on
        echo trace enabled
    else
        set -e fish_trace
        echo trace disabled
    end
end

# Fix winget command in WSL
if type -q powershell.exe && type -q winget.exe
    alias winget "winrun winget"
end

# Changes directory to home directory
function home
    if test "$PWD" = "$HOME" && type -q powershell.exe
        set -l home (string split \\ (string replace \r "" (winrun iex "echo \$env:USERPROFILE")))
        cd /mnt/(string lower (string replace : "" $home[1]))/(string join / $home[2..-1])
    else
        cd
    end
end

# Run a command through PowerShell
function winrun
    set -l cmd $argv[1]
    for arg in $argv[2..-1]
        set -a cmd "'$arg'"
    end
    powershell.exe -Command $cmd
end


# MoTD
function fish_greeting
    if type -q tmux && test -z "$TMUX"
        exec tmux new -A
    end
    if type -q fastfetch
        fastfetch -l windows
    end
end

# Disables displaying mode above the prompt
function fish_mode_prompt
end

# Custom prompt
function fish_prompt
    set -l stat $status

    if test $stat -ne 0
        echo \a
    end

    set_color brwhite
    echo -n $USER
    set_color brblack
    echo -n @
    set_color white
    echo -n "$(prompt_hostname) "

    set_color brcyan
    echo -n (prompt_pwd)

    set_color brblack
    echo (fish_vcs_prompt)

    if test $stat -ne 0
        set_color brred
        echo -n "$stat "
    else
        set_color brcyan
    end
    switch $fish_bind_mode
        case default
            echo -n λ
        case insert
            echo -n I
        case visual
            echo -n V
        case "*"
            echo -n R
    end
    echo -n " "
end

# Disables Vi mode
function fish_user_key_bindings
    fish_default_key_bindings
end
