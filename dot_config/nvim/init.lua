-- Plugin suite install
local pkg_path = vim.fn.stdpath("data") .. "/site/"
local mini_path = pkg_path .. "pack/deps/start/mini.nvim"
if not vim.loop.fs_stat(mini_path) then
	vim.fn.system({ "git", "clone", "--filter=blob:none", "https://github.com/echasnovski/mini.nvim", mini_path })
	vim.cmd("packadd mini.nvim | helptags ALL")
end

-- Plugin suite config
require("mini.comment").setup() -- Comment keybinds
require("mini.diff").setup() -- Diff visualizer
require("mini.files").setup({ -- File navigation
	mappings = { go_in = "<Enter>", go_out = "<Left>" },
	windows = { preview = true, width_focus = 30, width_nofocus = 30, width_preview = 30 },
})
require("mini.git").setup() -- Git info
require("mini.indentscope").setup({ symbol = "┃" }) -- Indent visualizer
require("mini.map").setup({ window = { winblend = 0 } }) -- Code map
MiniMap.open()
require("mini.move").setup({ -- Move keybinds
	mappings = {
		up = "<M-Up>",
		down = "<M-Down>",
		line_up = "<M-Up>",
		line_down = "<M-Down>",
	},
})
require("mini.notify").setup({ -- Notifications
	content = {
		format = function(notif)
			return notif.msg
		end,
	},
	window = { config = { border = "rounded", width = 40 }, winblend = 0 },
})
vim.notify = MiniNotify.make_notify()
require("mini.starter").setup() -- Start page
require("mini.statusline").setup() -- Statusline
require("mini.tabline").setup({ tabpage_section = "none" }) -- Tabline
require("mini.trailspace").setup() -- Highlight and delete trailing whitespace

-- Plugin manager setup
require("mini.deps").setup({ path = { package = pkg_path } })
local add = MiniDeps.add

-- Nerd font icons
add("nvim-tree/nvim-web-devicons")
require("nvim-web-devicons").setup()

-- Colorscheme
add("ellisonleao/gruvbox.nvim")
local gruvbox = require("gruvbox")
gruvbox.setup({ contrast = "hard", transparent_mode = true })
vim.cmd("colorscheme gruvbox")
vim.api.nvim_set_hl(0, "CmpItemKindCopilot", { fg = gruvbox.palette.light4 })

-- Better syntax highlighting
add("nvim-treesitter/nvim-treesitter")
require("nvim-treesitter.configs").setup({ ensure_installed = "all", highlight = { enable = true } })

-- Automatically unhighlight search
add("romainl/vim-cool")

-- Highlight undo operation
add("tzachar/highlight-undo.nvim")
require("highlight-undo").setup()

-- Formatting
add("stevearc/conform.nvim")
require("conform").setup({
	format_on_save = {
		lsp_format = "prefer",
		timeout_ms = 5000,
	},
	formatters_by_ft = {
		fish = { "fish_indent" },
		html = { "prettier" },
		lua = { "stylua" },
		rust = { "rustfmt" },
		toml = { "taplo" },
		yaml = { "prettier" },
	},
})

-- Better command mode wildmenu
add("gelguy/wilder.nvim")
local wilder = require("wilder")
wilder.setup({ modes = { ":" } })
wilder.set_option(
	"renderer",
	wilder.popupmenu_renderer(wilder.popupmenu_palette_theme({
		border = "rounded",
		highlighter = wilder.basic_highlighter(),
		highlights = {
			accent = wilder.make_hl("WilderAccent", "Pmenu"),
		},
		max_width = "50%",
	}))
)

-- Completion
add("hrsh7th/nvim-cmp")
add("L3MON4D3/LuaSnip")
add("hrsh7th/cmp-buffer")
add("hrsh7th/cmp-nvim-lsp-signature-help")
add("hrsh7th/cmp-nvim-lua")
add("onsails/lspkind.nvim")
add("saadparwaiz1/cmp_luasnip")
add("uga-rosa/cmp-dictionary")
local cmp = require("cmp")
cmp.setup({
	formatting = {
		format = require("lspkind").cmp_format({
			before = function(entry, vim_item)
				vim_item.menu = nil
				return vim_item
			end,
			mode = "symbol",
			symbol_map = { Copilot = "" },
		}),
	},
	window = {
		completion = cmp.config.window.bordered(),
		documentation = cmp.config.window.bordered(),
	},
	mapping = {
		["<M-c>"] = cmp.mapping.confirm({ select = true }), -- Confirm selection
		["<M-Down>"] = function() -- Next item
			cmp.select_next_item()
		end,
		["<M-Up>"] = function() -- Previous item
			cmp.select_prev_item()
		end,
	},
	sources = cmp.config.sources({
		{ name = "copilot" },
		{ name = "luasnip" },
		{ name = "nvim_lsp" },
		{ name = "nvim_lsp_signature_help" },
		{ name = "nvim_lua" },
		{ name = "buffer" },
		{ name = "dictionary" },
	}),
	snippet = {
		expand = function(args)
			require("luasnip").lsp_expand(args.body)
		end,
	},
})
require("cmp_dictionary").setup({ paths = { "/usr/share/dict/words" }, first_case_insensitive = true })

-- Github copilot
add("zbirenbaum/copilot.lua")
add("zbirenbaum/copilot-cmp")
require("copilot").setup({ suggestion = { enabled = false }, panel = { enabled = false } })
require("copilot_cmp").setup()
vim.cmd("Copilot auth")

-- LSPs
add("neovim/nvim-lspconfig")
add("hrsh7th/cmp-nvim-lsp")
local lsp = require("lspconfig")
local caps = require("cmp_nvim_lsp").default_capabilities()
lsp.rust_analyzer.setup({ capabilities = caps })
lsp.wgsl_analyzer.setup({ capabilities = caps })

-- Vim options
vim.o.cursorline = true -- Highlight the current line
vim.o.number = true -- Show line numbers
vim.o.scrolloff = 1000 -- Center cursor vertically
vim.o.sidescrolloff = 1000 -- Center cursor horizontally
vim.o.wrap = false -- Disable wrap

-- Vim tab options
local tab_width = 4 -- 4 wide
vim.o.expandtab = true -- Use spaces instead of tabs
vim.o.shiftwidth = tab_width -- Indent size
vim.o.softtabstop = tab_width -- Delete and add spaces like tabs
vim.o.tabstop = tab_width -- Tab display size

-- Vim variables
vim.g.mapleader = " "

-- Custom keybinds
vim.keymap.set("n", "<M-Left>", function() -- Previous buffer
	vim.cmd("bprevious")
end)
vim.keymap.set("n", "<M-Right>", function() -- Next buffer
	vim.cmd("bnext")
end)
vim.keymap.set("n", "<Leader>m", function() -- Merge line up
	vim.cmd(".-1substitute/\\n\\s*//")
	vim.cmd("nohlsearch")
end)
vim.keymap.set("n", "<Leader>r", vim.lsp.buf.rename) -- Rename symbol
vim.keymap.set("n", "<Leader>t", MiniFiles.open) -- Open file navigator
vim.keymap.set("n", "<Leader>x", function() -- Delete buffer
	vim.cmd("bdelete")
end)

-- Autocommands
vim.api.nvim_create_autocmd("BufWritePre", { -- Trim whitespace before saving
	callback = function()
		MiniTrailspace.trim()
		MiniTrailspace.trim_last_lines()
	end,
})
vim.api.nvim_create_autocmd("BufEnter", { -- Automatically enable copilot
	callback = function()
		vim.cmd("Copilot enable")
	end,
})
vim.api.nvim_create_autocmd("FileType", { -- Comment string for GLSL
	pattern = { "glsl" },
	callback = function()
		vim.bo.commentstring = "//%s"
	end,
})

-- Update function
function update()
	MiniDeps.later(function()
		vim.cmd("DepsUpdate")
	end)
	vim.api.nvim_create_autocmd("BufEnter", {
		pattern = "mini-deps://confirm-update",
		callback = function()
			vim.api.nvim_feedkeys(":write\n", "n", true)
			vim.cmd("DepsClean")
		end,
	})
	vim.cmd("TSUpdate")
end
